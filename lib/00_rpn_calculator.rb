# # RPN Calculator
#
# "RPN" stands for "Reverse Polish Notation". (See [the wikipedia
# entry](http://en.wikipedia.org/wiki/Reverse_Polish_notation) for
# more information on this colorful term.) Briefly, in an RPN world,
# instead of using normal "infix" notation, e.g.
#
#     2 + 2
#
# you use "postfix" notation, e.g.
#
#     2 2 +
#
# While this may seem bizarre, there are some advantages to doing
# things this way. For one, you never need to use parentheses, since
# there is never any ambiguity as to what order to perform operations
# in. The rule is, you always go from the back, or the left side.
#
#     1 + 2 * 3 =>
#     (1 + 2) * 3 or
#     1 + (2 * 3)
#
#     1 2 + 3 * => (1 + 2) * 3
#     1 2 3 * + => 1 + (2 * 3)
#
# Another advantage is that you can represent any mathematical formula
# using a simple and elegant data structure, called a
# [stack](http://en.wikipedia.org/wiki/Stack_(data_structure)).
#
# # Hints
#
# Ruby doesn't have a built-in stack, but the standard Array has all
# the methods you need to emulate one (namely, `push` and `pop`, and
# optionally `size`).
#
# See
# * <http://en.wikipedia.org/wiki/Reverse_Polish_notation>
# * <http://www.calculator.org/rpn.aspx>

class RPNCalculator
  def initialize
    # inputs should be an array of numbers and symbols denoting operators
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    syms = '+-*/'
    string.split.map.each do |char|
      if syms.include?(char)
        char.to_sym
      else
        char.to_i
      end
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  private

  def perform_operation(sym)
    raise 'calculator is empty' if @stack.length < 2

    second = @stack.pop
    first = @stack.pop

    if sym == :+
      @stack << first + second
    elsif sym == :-
      @stack << first - second
    elsif sym == :*
      @stack << first * second
    elsif sym == :/
      @stack << first.to_f / second.to_f
    else
      @stack << first
      @stack << second
      raise "No such operation: #{sym}"
    end
  end
end
